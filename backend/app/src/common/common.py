from flask import jsonify
import time

def answer(ok=True, data=None, message=None, error_code=400):
    return_dict = {'ok': ok}
    if message is not None:
        return_dict['message'] = message
    if data is not None:
        return_dict['data'] = data
    return jsonify(return_dict), error_code
