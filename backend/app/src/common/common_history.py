import time

def record_transaction(user, transaction, history_db):
    record = transaction.copy()
    record['email'] = user
    # its cheaper to do filtering using epoch time.
    record['timestamp'] = time.time()
    history_db.insert_one(record)


def perform_transaction(user, transaction, assets_db):
    if transaction['name'] in ('WITHDRAW', 'TRANSFER'):
        source_query = {'email': user, 'asset': transaction['source']}
        source = assets_db.find_one(source_query)
        source['amount'] = source['amount'] - transaction['amount']
        assets_db.update_one({"_id":source["_id"]},
                             {"$set": {"amount": source["amount"]}},
                             upsert=False)

    if  transaction['name'] in ('DEPOSIT', 'TRANSFER'):
        traget_query = {'email': user, 'asset': transaction['target']}
        target = assets_db.find_one(traget_query)
        target['amount'] = target['amount'] + transaction['amount']
        assets_db.update_one({"_id":target["_id"]},
                             {"$set": {"amount": target["amount"]}},
                             upsert=False)
