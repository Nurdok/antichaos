from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError

schema = {
    "definitions": {
        "share": {
           "type": "object",
           "properties": {
              "name": {
                  "type": "string"
              },
              "symbol": {
                  "type": "string"
              },
              "number": {
                  "type": "number"
              },
              "fee": {
                  "type": "number"
              },
              "quota": {
                  "type": "number"
              },
           },
           "additionalProperties": False
        },
        "fund" : {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "quota": {
                    "type": "number"
                },
                "shares_allocation": {
                  "type": "array",
                  "items": { "$ref": "#/definitions/share" },
                  "default": []
                }
            },
            "additionalProperties": False
        },
        "contract" : {
            "type": "object",
            "properties": {
                "description": {
                    "type": "string"
                },
                 "funds_allocation": {
                  "type": "array",
                  "items": { "$ref": "#/definitions/fund" },
                  "default": []
                },
                "date": {
                    "type": "number"
                }
            },
            "additionalProperties": False
        }
    }
}

def validate_contract(data):
    try:
        validate(data, schema)
    except (ValidationError, SchemaError) as e:
        return {'ok': False, 'message': e}

    return {'ok': True, 'data': data}
