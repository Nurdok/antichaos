from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError

assets_schema = {
    "type": "object",
    "properties": {
        "asset": {
            "type": "string"
        },
        "amount": {
            "type": "number"
        }
    },
    "additionalProperties": False
}



def validate_assets(data):
    try:
        validate(data, assets_schema)
    except (ValidationError, SchemaError) as e:
        return {'ok': False, 'message': e}

    return {'ok': True, 'data': data}
