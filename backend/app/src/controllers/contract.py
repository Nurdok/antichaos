''' controller and routes for user settings '''
import os
from flask import request
from src.common.common import answer
from src.schemas.contract import validate_contract
from src.log import log as logger
from src import app, mongo
from flask_jwt_extended import (jwt_required,
                                get_jwt_identity)


ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@app.route('/data/set_contract', methods=['POST'])
@jwt_required
def set_contract():
    current_user = get_jwt_identity()
    bad_request_parameters = answer(ok=True,
                                    message='Bad request parameters!',
                                    error_code=400)

    input_data = validate_contract(request.get_json())
    if not(input_data['ok']):
        message = 'Bad request parameters: {}'.format(input_data['message'])
        return answer(ok=False, message=message, error_code=400)

    data = {'$set': {'contract': input_data['data']}}
    query = {'email': current_user['email']}
    try:
        LOG.info(f'inserting contract {input_data}')
        mongo.db.contracts.update_one(query, data, upsert=True)
    except:
        return bad_request_parameters

    return answer(ok=True, message='success', error_code=200)


@app.route('/data/get_contract', methods=['GET'])
@jwt_required
def get_contract():
    current_user = get_jwt_identity()
    raw_data = request.get_json()
    data = mongo.db.contracts.find_one({'email': current_user['email']})
    LOG.info(f'read {data} for user: {current_user["email"]}')
    response = data.get('contract', {}) if data else {}
    return answer(ok=True, data=response, error_code=200)
