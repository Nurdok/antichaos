''' controller and routes for users '''
import os
from flask import request, jsonify
from src.common.common import answer
from src.schemas.users import validate_user
from src.log import log as logger
from src import app, mongo, flask_bcrypt, jwt
from flask_jwt_extended import (create_access_token,
                                create_refresh_token,
                                jwt_required,
                                jwt_refresh_token_required,
                                get_jwt_identity)

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@app.route('/data/register', methods=['POST'])
def register():
    ''' register user endpoint '''
    LOG.info("register new user")

    data = validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        data['password'] = flask_bcrypt.generate_password_hash(
                            data['password'])
        mongo.db.users.insert_one(data)
        message = 'User created succesfully!'
        ok = True
        error_code = 200
    else:
        message = 'Bad request parameters: {}'.format(data['message'])
        ok = False
        error_code = 400

    return answer(ok=ok, message=message, error_code=error_code)


@app.route('/data/auth', methods=['POST'])
def auth_user():
    ''' auth endpoint '''
    LOG.info('authenticate user')

    data = validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        user = mongo.db.users.find_one({'email': data['email']}, {"_id": 0})
        if user and flask_bcrypt.check_password_hash(user['password'], data['password']):
            del user['password']
            access_token = create_access_token(identity=data)
            refresh_token = create_refresh_token(identity=data)
            user['token'] = access_token
            user['refresh'] = refresh_token
            return answer(ok=True, data=user, error_code=200)
        else:
            return answer(ok=False, message='invalid username or password', error_code=401)

    else:
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)


@app.route('/data/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    ''' refresh token endpoint '''
    current_user = get_jwt_identity()
    LOG.debug('refresh token for user {}'.format(current_user))

    ret = {
            'token': create_access_token(identity=current_user)
    }
    return answer(ok=True, data=ret, error_code=200)


@app.route('/data/user', methods=['GET', 'DELETE', 'PATCH'])
@jwt_required
def user():
    ''' route read user '''
    LOG.debug("user command {}".format(request.method))

    if request.method == 'GET':
        query = request.args
        data = mongo.db.users.find_one(query, {"_id": 0})
        return answer(ok=True, data=data, error_code=200)

    bad_request_parameters = answer(ok=True,
                                    message='Bad request parameters!',
                                    error_code=400)
    data = request.json()
    if request.method == 'DELETE':
        if data.get('email', None) is not None:
            db_response = mongo.db.users.delete_one({'email': data['email']})
            if db_response.deleted_count == 1:
                message = 'record deleted'
            else:
                message = 'no record found'

            return answer(ok=True, message=message, error_code=200)
        else:
            return bad_request_parameters

    if request.method == 'PATCH':
        if data.get('query', {}) != {}:
            mongo.db.users.update_one(data['query'], {'$set':
                data.get('payload', {})})
            return answer(ok=True, message='record updated', error_code=200)
        else:
            return bad_request_parameters


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return answer(ok=False, message='Missing Authorization Header', error_code=401)
