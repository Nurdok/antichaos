import React from "react";
import { connect } from 'react-redux';
import Layout from '../Layout/Layout';
import Grid from '@material-ui/core/Grid';
import CardHeader from '@material-ui/core/CardHeader';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { PercentStackedBarChart } from "@opd/g2plot-react";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import SharesTable from './Shares';
import MaterialTable from 'material-table';
import { contractActions } from '../actions/contract';
import fromUnixTime from 'date-fns/fromUnixTime';
import getUnixTime from 'date-fns/getUnixTime'
import Paper from '@material-ui/core/Paper';


class ContractPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open_asset_allocation: false,
            asset_allocation: [],
            description: 'Describe your ulysses pact.',
            date: null,
            amount: 10000,
        };
    }

    render_asset_allocation_selection() {
        console.log('this');
        return (
            <Dialog
                open={this.state.open_asset_allocation}
                onClose={_ => this.setState({open_asset_allocation: false})}>
                <DialogTitle id="allocate">Asset Allocation</DialogTitle>
                <DialogContent style={{display: 'flex', flexDirection: 'column'}}>
                  <DialogContentText>
                    Please select the asset allocation between stocks and bonds.
                  </DialogContentText>
                    <MaterialTable
                      columns={[{ title: 'Type', field: 'name' },
                                { title: 'Quota', field: 'quota', type: 'numeric'}]}
                      data={this.state.asset_allocation.map(
                          x =>
                            {
                                return { name: x.name, quota: x.quota};
                            })}
                      editable={{
                      onRowAdd: newData =>
                        new Promise((resolve, reject) => {
                          setTimeout(() => {
                            {
                              let data = this.state.asset_allocation;
                              if (data.map(x => x.name).includes(newData.name)) {
                                  return reject();
                              }
                              data.push({ name:newData.name,
                                          quota: Number(newData.quota),
                                          shares_allocation: []});
                              this.setState({ asset_allocation: data }, () => resolve());
                            }
                            resolve()
                          }, 1000)
                        }),
                      onRowUpdate: (newData, oldData) =>
                        new Promise((resolve, reject) => {
                          setTimeout(() => {
                            {
                              let data = this.state.asset_allocation;
                              const original = data.find(x => x.name === oldData.name);
                              const index = data.indexOf(original);
                              data[index] = {
                                          name:newData.name,
                                          quota: Number(newData.quota),
                                          shares_allocation: original.shares_allocation};
                              this.setState({ asset_allocation: data }, () => resolve());
                            }
                            resolve()
                          }, 1000)
                        }),
                      onRowDelete: oldData =>
                        new Promise((resolve, reject) => {
                          setTimeout(() => {
                            {
                              let data = this.state.asset_allocation;
                              const index = data.findIndex(x => x.name === oldData.name);
                              data.splice(index, 1);
                              this.setState({ asset_allocation:data }, () => resolve());
                            }
                            resolve()
                          }, 1000)
                        }),
                    }}
                    />
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={_ => { this.setState({ open_asset_allocation: false}) }} color="primary">
                        close
                      </Button>
                    </DialogActions>
                </Dialog>);
    }

    render_allocation() {
        let data = [];
        for (let x of this.state.asset_allocation) {
            data.push({type: x.name, value: x.quota, dummy: 'Allocation'});
        }
        const config = {
          forceFit: true,
          title: {
            visible: true,
            text: "Investment Allocation"
          },
          data: data,
          xField: 'value',
          yField: 'dummy',
          color: ['#2582a1', '#f88c24', '#c52125', '#87f4d0'],
        };

        return (
            <div>
            <Card>
                <CardActionArea
                    onClick={_=> {this.setState({open_asset_allocation: true})}}
                >
                <CardContent>
                { this.state.asset_allocation.length === 0?
                    <PercentStackedBarChart {...config} /> :
                    <PercentStackedBarChart {...config} stackField='type'/>
                }
                </CardContent>
                </CardActionArea>
            </Card>
            {this.state.open_asset_allocation && this.render_asset_allocation_selection()}
            </div>
        );
    }

    componentDidMount() {
        this.props.dispatch(contractActions.getContract());
    }

    componentDidUpdate(PrevProps, PrevStates) {
        const { add_contract: prev_add, contract: prev_contract } = PrevProps;
        const { add_contract: next_add, contract: next_contract } =  this.props;
        if (prev_add.adding && next_add.added) {
            this.props.dispatch(contractActions.getContract());
        }

        if (prev_contract.loading && next_contract.contract) {
            let contract = next_contract.contract;
            this.setState({...contract});
        }
    }

    UpdateShares(name, shares) {
        const data = this.state.asset_allocation;
        let original = data.find(x => x.name === name);
        const index = data.indexOf(original);
        data[index] = { name: name, quota: original.quota, shares_allocation: shares};
        this.setState({ asset_allocation: data });
    }

    render_total() {
        let total_quota = this.state.asset_allocation.reduce(
            (initial, item) => {return initial + item.quota},
            0);
        // inside share quota is actual percentage number.
        let data = [];
        let fee = 0;
        console.log(this.state.asset_allocation);
        console.log('total quota is ' + total_quota);
        for (let x of this.state.asset_allocation) {
            for (let s of x.shares_allocation) {
                const percent = s.quota * (x.quota / total_quota);
                console.log('x quota:' + x.quota);
                console.log('s quota:' + s.quota);
                data.push({
                    type: x.name,
                    name: s.name,
                    symbol: s.symbol,
                    number: s.number,
                    fee: s.fee,
                    percent: percent,
                    amount: this.state.amount * (percent / 100)
                })
                fee = fee + s.fee * (percent/100);
            }
        }

        return(
            <Card>
                <CardContent>
                    <Grid container spacing={3} direction="column">
                    <Grid item>
                        <TextField
                            autoFocus
                            label="Investment Amount"
                            type="number"
                            value={this.state.amount}
                            onChange={e => this.setState({ amount : Number(e.target.value)})}
                         />
                    </Grid>
                    <Grid item>
                    <MaterialTable
                      title="Total shares"
                      columns={[
                        { title: 'Type', field: 'type' },
                        { title: 'Name', field: 'name' },
                        { title: 'Symbol', field: 'symbol' },
                        { title: 'No.', field: 'number' },
                        { title: 'Fee', field: 'fee' },
                        { title: 'Percent', field: 'percent' },
                        { title: 'Amount', field: 'amount' },
                      ]}
                  data={data}
                  options={{
                    exportButton: true
                  }}
                />
                </Grid>
                <Grid item>
                    <Typography>
                    Weighted Fee is : {fee.toLocaleString() }
                    </Typography>
                </Grid>
                </Grid>
                </CardContent>
            </Card>
            );
    }

    render() {
        console.log('wow');
        return (
            <Layout PageName="Investment Contract">
                <section>
                <Grid
                container
                justify="center"
                spacing={3}>
                    { this.state.date &&
                        (<Grid item md={10}>
                            <Typography>
                            { "Signed " + fromUnixTime(this.state.date).toLocaleString() }
                            </Typography>
                            </Grid>)}
                    <Grid item md={5}>
                        <Card>
                            <CardContent>
                            <TextField
                            id="outlined-multiline-static"
                            label="Description"
                            multiline
                            fullWidth
                            value={this.state.description}
                            onChange={e =>{ this.setState({description: e.target.value})} }
                            variant="outlined" />
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item md={5}>
                    { this.render_allocation() }
                    </Grid>
                    {
                        this.state.asset_allocation.map( x => (
                            <Grid item md={10}>
                             <SharesTable
                                name={x.name}
                                shares={x.shares_allocation}
                                UpdateShares={shares => this.UpdateShares(x.name, shares)} />
                            </Grid>)
                        )
                    }
                    <Grid item md={10}>
                    { this.render_total() }
                    </Grid>
                    <Grid item md={3}>
                        <Button
                        onClick={ _ => {
                            this.props.dispatch(
                                contractActions.addContract(
                                    {
                                        description: this.state.description,
                                        asset_allocation: this.state.asset_allocation,
                                        date: getUnixTime(new Date())
                                    }
                                ))}}
                        color="primary">
                          Sign Contract
                        </Button>
                    </Grid>
                  </Grid>
                </section>
            </Layout>
        );
    }
}

function mapStateToProps(state) {
    const {  contract, add_contract } = state;
    return {
        contract,
        add_contract,
    };
}

export default connect(mapStateToProps)(ContractPage);
