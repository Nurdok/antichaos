import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from "@material-ui/core/Grid";
import Hidden from '@material-ui/core/Hidden';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Tooltip from '@material-ui/core/Tooltip';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
    marginTop: theme.spacing.unit * 2,
  },
  table: {
    minWidth: 700,
  },
  header: {
    width: "fit-content",
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.info.light,
    color: "white",
    "& svg": {
     margin: theme.spacing(1.5)
    },
    "& hr": {
     margin: theme.spacing(0, 0.5)
    }
  }
});

class AddShare extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          name: '',
          symbol: '',
          number: 0,
          fee: 0,
          quota: this.props.remaining_quota,
      }

      this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd() {
    this.props.handleAdd(this.state);
  }

  render() {
      const { fullScreen } = this.props;
      return (
        <div>
          <Dialog
            fullScreen={fullScreen}
            open={this.props.open}
            onClose={this.props.handleClose}
            aria-labelledby="add-share-dialog"
          >
            <DialogTitle id="add-share">Add Share</DialogTitle>
            <DialogContent style={{display: 'flex', flexDirection: 'column'}}>
              <DialogContentText>
                Please enter the Share properties and quota.
              </DialogContentText>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="flex-start"
                spacing={3}>
                <Grid item>
                  <TextField
                    autoFocus
                    id="name"
                    label="Name"
                    onChange={e => this.setState({ name : e.target.value})}
                    type="text"
                  />
              </Grid>
              <Grid item>
                  <TextField
                    autoFocus
                    id="symbol"
                    label="Symbol"
                    onChange={e => this.setState({ symbol : e.target.value})}
                    type="text"
                  />
              </Grid>
              <Grid item>
                  <TextField
                    autoFocus
                    id="number"
                    label="No."
                    onChange={e => this.setState({ number : Number(e.target.value)})}
                    type="number"
                  />
              </Grid>
              <Grid item>
                  <TextField
                    autoFocus
                    id="fee"
                    label="Fee"
                    onChange={e => this.setState({ fee : Number(e.target.value)})}
                    type="number"
                  />
              </Grid>
              <Grid item>
                  <TextField
                    autoFocus
                    id="quota"
                    label="Quota"
                    value={ this.state.quota }
                    onChange={e => this.setState({ quota : Number(e.target.value)})}
                    type="number"
                  />
              </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.handleAdd} color="primary">
                Add
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      );
    }
}

AddShare.propTypes = {
  open: PropTypes.element.isRequired,
  handleClose: PropTypes.element.isRequired,
  handleAdd: PropTypes.element.isRequired,
  remaining_quota: PropTypes.element.isRequired
};

class SharesTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            add_share_dialog: false,
        };

        this.deleteShare = this.deleteShare.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.render_mobile = this.render_mobile.bind(this);
        this.render_add_dialog = this.render_add_dialog.bind(this);
        this.render_desktop = this.render_desktop.bind(this);
    }

    deleteShare(name) {
        this.props.UpdateShares(
            this.props.shares.filter(
                share => { return share.name != name }));
    }

    handleAdd(share) {
        this.props.UpdateShares([...this.props.shares, share]);
        this.handleClose();
    }

    handleClose() {
        this.setState({add_share_dialog: false});
    }

    render_mobile(classes, shares) {
        return (
            <div>
            {shares && shares.map(n => {
             return (
                 <ExpansionPanel>
                   <ExpansionPanelSummary
                     expandIcon={<ExpandMoreIcon />}
                     aria-controls={"panel" + n.id + "a-content"}
                     id={"panel" + n.id + "a-header"}
                   >
                     <Typography className={classes.heading}>
                        {n.name + "    " + n.quota}
                     </Typography>
                   </ExpansionPanelSummary>
                   <ExpansionPanelDetails>
                     <Grid container spacing={2}>
                        <Grid item>
                        { "No.: " + n.number }
                        </Grid>
                        <Grid item>
                        { "Symbol: " + n.symbol}
                        </Grid>
                        <Grid item>
                        { "Fee: " + n.fee}
                        </Grid>
                     </Grid>
                   </ExpansionPanelDetails>
                   <ExpansionPanelActions>
                       <Fab
                           color="secondary"
                           size="small"
                           aria-label="Remove Expense"
                           onClick={_ => this.deleteShare(n.name, n.quota)}>
                         <Icon> delete_icon </Icon>
                       </Fab>
                   </ExpansionPanelActions>
                 </ExpansionPanel>
              );
            })}
            </div>
        );
    }

    render_add_dialog() {
        let quota = 100 - this.props.shares.reduce(
            (total, share) => {
                return total + share.quota;
                },
            0);
        return (
            <AddShare
                remaining_quota={quota}
                open={this.state.add_share_dialog}
                handleAdd={this.handleAdd}
                handleClose={this.handleClose}
            />

        );
    }

    render_desktop(classes, shares) {
        console.log('render desktop');
        let weighted = shares.reduce((total, share) => {
            return total + ((share.quota / 100) * share.fee)
        }, 0);
        return (
            <Grid container direction="column">
            <Grid item>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Symbol</TableCell>
                  <TableCell align="right">No.</TableCell>
                  <TableCell align="right">Fee</TableCell>
                  <TableCell align="right">Quota</TableCell>
                  <TableCell align="right">Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {shares.map(s => {
                 return (
                    <TableRow>
                      <TableCell>{s.name}</TableCell>
                      <TableCell align="right">{s.symbol}</TableCell>
                      <TableCell align="right">{s.number}</TableCell>
                      <TableCell align="right">{s.fee}</TableCell>
                      <TableCell align="right">{s.quota}</TableCell>
                      <TableCell align="right">
                          <Fab
                              color="secondary"
                              size="small"
                              aria-label="Remove Expense"
                              onClick={_ => this.deleteShare(s.name, s.quota)}>
                            <Icon> delete_icon </Icon>
                          </Fab>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
            </Grid>
            <Grid item>
            <Typography> Weighted fee: {weighted.toLocaleString()} </Typography>
            </Grid>
            </Grid>
        );
    }


    render() {
        console.log('this is here')
        const { classes, shares, name } = this.props;
        return (
        <div>
          <Paper className={classes.root}>
              <Toolbar>
                    <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                      {name}
                    </Typography>
                    <Tooltip title="add">
                      <IconButton aria-label="add"
                        onClick={ _ => {
                                this.setState({add_share_dialog: true})} }>
                        <AddIcon />
                      </IconButton>
                    </Tooltip>
                </Toolbar>
              <Hidden xsDown implementation="css">
                { this.render_desktop(classes, shares) }
              </Hidden>
              <Hidden smUp implementation="css">
                { this.render_mobile(classes, shares) }
              </Hidden>
          </Paper>
          { this.state.add_share_dialog && this.render_add_dialog() }
          </div>
        );
    }
}


SharesTable.propTypes = {
  classes: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  shares: PropTypes.object.isRequired,
  UpdateShares: PropTypes.func.isRequired,
};


export default withStyles(styles, { withTheme: true })(SharesTable);
