import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Grid from '@material-ui/core/Grid';

import { transactionActions } from '../actions/transaction'


class SimpleTransactionDialog extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          amount: 0,
      }

      this.handleTransaction = this.handleTransaction.bind(this);
  }

  handleTransaction() {
      this.props.handleClose();
      const { Transaction, AssetName, dispatch, TransactionName } = this.props;
      const { amount } = this.state;
      // TODO: verify name isn't empty. else send alert
      dispatch(Transaction(AssetName, amount, "manual " + TransactionName));
  }

  render() {
      const { AssetName, TransactionName, fullScreen } = this.props;
      return (
        <div>
          <Dialog
            fullScreen={fullScreen}
            open={this.props.open}
            onClose={this.props.handleClose}
            aria-labelledby="${TransactionName}-dialog"
          >
            <DialogTitle id={TransactionName}>{TransactionName}</DialogTitle>
            <DialogContent style={{display: 'flex', flexDirection: 'column'}}>
              <DialogContentText>
                {AssetName} :Please select the amount to {TransactionName}.
              </DialogContentText>
              <TextField
                autoFocus
                id="amount"
                label={TransactionName + ' amount'}
                onChange={e => this.setState({ amount : Number(e.target.value)})}
                type="number"
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.handleTransaction} color="primary">
                {TransactionName}
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      );
    }
}

SimpleTransactionDialog.propTypes = {
  open: PropTypes.element.isRequired,
  handleClose: PropTypes.element.isRequired,
  AssetName: PropTypes.element.isRequired,
  Transaction: PropTypes.element.isRequired,
  TransactionName: PropTypes.element.isRequired
};

const connectSimpleTransactionDialog = withMobileDialog()(connect()(SimpleTransactionDialog));
export { connectSimpleTransactionDialog as SimpleTransactionDialog };


class TransferDialog extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          amount: 0,
          target_asset: '',
      }

      this.handleTransfer = this.handleTransfer.bind(this);
  }

  handleTransfer() {
    this.props.handleClose();
    const { AssetName, dispatch } = this.props;
    const { amount, target_asset } = this.state;
    // TODO: verify name isn't empty. else send alert
    dispatch(transactionActions.transfer(AssetName,
                                         target_asset,
                                         amount,
                                         'manual transfer'));
  }

  render() {
      const { AssetName, assets, fullScreen } = this.props;
      return (
        <div>
          <Dialog
            fullScreen={fullScreen}
            open={this.props.open}
            onClose={this.props.handleClose}
            aria-labelledby="transfer-dialog"
          >
            <DialogTitle id="transfer">Transfer</DialogTitle>
            <DialogContent style={{display: 'flex', flexDirection: 'column'}}>
                <Grid
                container
                direction="column"
                justify="center"
                alignItems="flex-start"
                spacing={3}>
                <Grid item>
              <DialogContentText>
                {AssetName} :Please select the amount to transfer, and to whom.
              </DialogContentText>
              </Grid>
              <Grid item>
              <TextField
                autoFocus
                id="amount"
                label='Transfer amount'
                onChange={e => this.setState({ amount : Number(e.target.value)})}
                type="number"
              />
              </Grid>
              <Grid item>
            <Select
              value={this.state.target_asset}
              onChange={e => this.setState({ target_asset : e.target.value})}
              inputProps={{
                name: 'target-asset',
                id: 'target-asset',
              }}
            >
                {assets.items && Object.keys(assets.items).filter(
                            asset => asset !== AssetName).map(
                                asset => (<MenuItem value={asset}>{asset}</MenuItem>))
                }
            </Select>
            </Grid>
            </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.handleTransfer} color="primary">
                Transfer
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      );
    }
}

TransferDialog.propTypes = {
  open: PropTypes.element.isRequired,
  handleClose: PropTypes.element.isRequired,
  AssetName: PropTypes.element.isRequired
};

function mapStateToProps(state) {
    const { assets } = state;
    return {
        assets,
    };
}

const connectTransferDialog = withMobileDialog()(connect(mapStateToProps)(TransferDialog));
export { connectTransferDialog as TransferDialog };
