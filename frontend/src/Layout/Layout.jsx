import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SettingsIcon from '@material-ui/icons/Settings';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },

  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
});

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileOpen: false,
    };

    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
  }

  handleDrawerToggle() {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  }


  render() {
      const { classes, user } = this.props;

      const drawer = (
        <div>
          <List className={classes.toolbar}>
            <ListItem component="a">
                <ListItemText primary={user.data.name} />
                <ListItemSecondaryAction>
                    <IconButton
                      color="inherit"
                      aria-label="settings"
                      href="/settings"
                    >
                      <SettingsIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
          </List>
          <Divider />
          <List>
              <ListItemLink href="/" className={classes.nested}>
                <ListItemText primary="Monthly Expenses" />
              </ListItemLink>
              <ListItemLink href="/assets">
                <ListItemText primary="Assets" />
              </ListItemLink>
              <ListItemLink href="/report_annual_expanses" className={classes.nested}>
                <ListItemText primary="Annual Report" />
              </ListItemLink>
              <ListItemLink href="/periodic_transactions">
                <ListItemText primary="Transaction" />
              </ListItemLink>
              <ListItemLink href="/independence">
                <ListItemText primary="Independence" />
              </ListItemLink>
              <ListItemLink href="/contract">
                <ListItemText primary="Investment Contract" />
              </ListItemLink>
              <Divider />
              <ListItemLink href="/login">
                <ListItemText primary="Logout" />
              </ListItemLink>
          </List>
        </div>
      );

      return (
        <div className={classes.root}>
          <AppBar
            position="fixed"
            className={classNames(
                        classes.appBar,
                        { [classes.appBarShift]: this.state.mobileOpen, })}>
            <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
              <Typography variant="h6" color="inherit" className={classes.grow}>
                {this.props.PageName}
              </Typography>
              {this.props.AppBarIcons()}
            </Toolbar>
          </AppBar>
          <nav className={classes.drawer}>
            {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
            <Hidden smUp implementation="css">
              <Drawer
                container={this.props.container}
                variant="temporary"
                open={this.state.mobileOpen}
                onClose={this.handleDrawerToggle}
                classes={{
                  paper: classes.drawerPaper,
                }}
                ModalProps={{
                  keepMounted: true, // Better open performance on mobile.
                }}
              >
                {drawer}
              </Drawer>
            </Hidden>
            <Hidden xsDown implementation="css">
              <Drawer
                classes={{
                  paper: classes.drawerPaper,
                }}
                variant="permanent"
                open
              >
                {drawer}
              </Drawer>
            </Hidden>
          </nav>
          <main className={classes.content}>
          <div className={classes.toolbar} />
          {this.props.children}
          </main>
        </div>
      );
  }
}


Layout.propTypes = {
  classes: PropTypes.object.isRequired,
  PageName: PropTypes.object.isRequired,
  AppBarIcons: PropTypes.object.isRequired,
};


Layout.defaultProps = {
  AppBarIcons: e => <div name="no_icons"/>,
};


function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user,
    };
}

export default withStyles(styles)(connect(mapStateToProps)(Layout));
