import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import MenuItem from '@material-ui/core/MenuItem';

import { expenseCategories } from '../constants/expense'
import { expenseActions } from '../actions/expense';

import fromUnixTime from 'date-fns/fromUnixTime'
import getUnixTime from 'date-fns/getUnixTime'
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import Select from '@material-ui/core/Select';

class AddExpenseDialog extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          name: '',
          amount: 0,
          category: '',
          date: getUnixTime(new Date()),
      }

      this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd() {
    this.props.handleClose();
    const { dispatch } = this.props;
    const { name, amount, category, date } = this.state;
    // TODO: verify name isn't empty. else send alert
    dispatch(expenseActions.addExpense(name, amount, category, date));
  }

  render() {
      const { fullScreen } = this.props;
      return (
        <div>
          <Dialog
            fullScreen={fullScreen}
            open={this.props.open}
            onClose={this.props.handleClose}
            aria-labelledby="add-expense-dialog"
          >
            <DialogTitle id="add-expense">Add Expense</DialogTitle>
            <DialogContent style={{display: 'flex', flexDirection: 'column'}}>
              <DialogContentText>
                Please select the Name, Amount, Category and date of the expense.
              </DialogContentText>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="flex-start"
                spacing={3}>
                <Grid item>
                  <TextField
                    autoFocus
                    id="name"
                    label="expense name"
                    onChange={e => this.setState({ name : e.target.value})}
                    type="text"
                  />
                </Grid>
                <Grid item>
                  <TextField
                    autoFocus
                    id="amount"
                    label="new expense amount"
                    onChange={e => this.setState({ amount : Number(e.target.value)})}
                    type="number"
                  />
                </Grid>
                <Grid item>
                  <Select
                    value={this.state.target_asset}
                    onChange={e => this.setState({ category : e.target.value})}
                    inputProps={{
                      name: 'Category',
                      id: 'category',
                    }}
                  >
                      { expenseCategories.categories.map(
                                      category => (<MenuItem value={category}>{category}</MenuItem>))
                      }
                  </Select>
                </Grid>
                <Grid item>
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid container justify="space-around">
                      <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="MM/dd/yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          label="Date"
                          value={fromUnixTime(this.state.date)}
                          onChange={e => this.setState({ date : getUnixTime(e)})}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                    </Grid>
                    </MuiPickersUtilsProvider>
                  </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.handleAdd} color="primary">
                Add
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      );
    }
}

AddExpenseDialog.propTypes = {
  open: PropTypes.element.isRequired,
  handleClose: PropTypes.element.isRequired
};

const connectedDialog = connect()(withMobileDialog()(AddExpenseDialog));
export default connectedDialog;
