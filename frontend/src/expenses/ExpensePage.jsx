import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AddExpenseDialog from './AddExpense';
import ExpenseTable from './ExpenseTable';
import Layout from '../Layout/Layout';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';

export default class MonthlyExpanseReport extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };

        this.renderAddButton = this.renderAddButton.bind(this);
        this.handleOpenAdd = this.handleOpenAdd.bind(this);
        this.handleCloseAdd = this.handleCloseAdd.bind(this);
    }
    handleOpenAdd() {
      this.setState({ open: true });
    }

    handleCloseAdd() {
      this.setState({ open: false });
    }
    renderAddButton() {
        return (
            <div>
                <AddExpenseDialog
                    open={this.state.open}
                    handleClose={this.handleCloseAdd} />
                <Fab
                    color="secondary"
                    size="small"
                    aria-label="Add Asset"
                    onClick={e => this.handleOpenAdd()}>
                  <Icon> add_icon </Icon>
                </Fab>
            </div>
        )
    }

    render() {
        return (
            <Layout PageName="Monthly Expenses" AppBarIcons={this.renderAddButton}>
                <ExpenseTable />
            </Layout>
        )
    }
}
