import React from "react";
import { LineChart } from "@opd/g2plot-react";
import { expenseActions } from "../actions/expense";
import { settingsActions } from "../actions/settings";
import { connect } from 'react-redux';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Layout from '../Layout/Layout';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const config = {
  title: {
    visible: true,
    text: "Monthly Expense"
  },
  description: {
    visible: true,
    text: "Monthly Expenses by categories"
  },
  legend: {
      position: "bottom"
  },
  padding: "auto",
  forceFit: true,
  xField: "month",
  yField: "value",
  smooth: true,
  seriesField: "category",
  meta: {
    month: {
      alias: "Month"
    },
    value: {
      alias: "ILS",
      formatter: (value)=>{ return value.toLocaleString() }
    }
  }
};

class ExpenseChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            present: [],
            inactive_categories: [],
            all_categories: [],
            max_month: 0,
            year: 2020,
            agg: false,
            display_budget: false,
            display_category_graphs: false,
        };

        this.changeCategory = this.changeCategory.bind(this);
        this.render_categories = this.render_categories.bind(this);
        this.render_single_category = this.render_single_category.bind(this);
        this.add_budget_data = this.add_budget_data.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(expenseActions.getMonthlyExpenses(this.state.year));
        this.props.dispatch(settingsActions.get_budget());
    }

    componentDidUpdate(PrevProps, PrevStates) {
        console.log('start update chart');
        // Get info from server
        if (PrevStates.year != this.state.year) {
            console.log('prevprops date: ' + PrevProps.year );
            console.log('current date: ' + this.state.year );
            this.props.dispatch(
                expenseActions.getMonthlyExpenses(this.state.year));
        }

        const { monthly_expenses: prev } = PrevProps;
        const { monthly_expenses: next } = this.props;

        // reset present to show only relevant data
        if ((prev.loading == true && next.items ||
             PrevStates.inactive_categories != this.state.inactive_categories ||
             PrevStates.display_category_graphs != this.state.display_category_graphs)
            && next.items) {
            console.log('updating presentable data.');
            let present = [];
            let all_categories = new Set();
            let max_month = this.state.max_month;
            console.log('max month before start: ' + max_month);
            for (let month in next.items) {
                let total = 0;
                let active_categories = new Set();
                for (let [category, amount] of Object.entries(next.items[month])) {
                    all_categories.add(category);
                    if (!this.state.inactive_categories.includes(category)) {
                        active_categories.add(category);
                        total += amount;
                        if (this.state.display_category_graphs) {
                            present.push({
                                month: month,
                                value: amount,
                                category: category });
                        }
                    }
                }
                if (this.state.display_category_graphs) {
                    let missing = new Set(
                        [...all_categories].filter(
                            x => !(this.state.inactive_categories.includes(x) ||
                                    active_categories.has(x))))
                    for (let category of missing) {
                        present.push({
                            month: month, value: 0, category: category });
                    }
                }
                max_month = Math.max(max_month, month);
                present.push({ month: month, value: total, category: 'total' });
            }
            this.setState({ present: present, max_month: max_month });
            this.setState({ all_categories: [...all_categories].sort()});
        }
    }

    changeCategory(category, remove) {
        if (remove) {
          this.setState({
              inactive_categories: [...this.state.inactive_categories, category]});
        } else {
          this.setState({
              inactive_categories: this.state.inactive_categories.filter(
                  e => e != category)});
        }
    }

    render_single_category(category) {
        return (
            <FormControlLabel
              control={
                  <Switch
                    checked={!this.state.inactive_categories.includes(category)}
                    onChange={e => this.changeCategory(category,
                                                       !e.target.checked)}
                    name={category} />}
              label={category}
            />
        );
    }

    render_categories() {
        return (
            <Card>
                <CardContent>
                    <FormGroup>
                     <FormLabel component="legend">Displayed Categories</FormLabel>
                    { this.state.all_categories.map(
                        category => this.render_single_category(category)) }
                    </FormGroup>
                </CardContent>
            </Card>
        );
    }

    add_budget_data(budget) {
        console.log('max month before evaluation: ' + this.state.max_month);
        return Array.from(Array(this.state.max_month).keys()).map(
            m => ({ month: (m + 1).toString() , value: budget, category:'Budget' }));
    }

    aggregate(data, agg) {
        if (!agg) {
            return data;
        }
        let current = {};
        let new_data = []
        data.sort((a, b) => a.month - b.month);
        for (let p of data) {
            let cumulative = current[p.category] || 0;
            cumulative += p.value;
            new_data.push({month: p.month, value: cumulative, category: p.category});
            current[p.category] = cumulative;
        }
        return new_data;
    }

    render_agg_buttons(){
        return (
        <Card>
        <CardContent>
            <FormGroup>
            <FormControlLabel
              control={
                  <Switch
                    checked={this.state.agg}
                    onChange={e => this.setState({ agg: e.target.checked })}
                    name="Aggregate" />}
              label="Aggregate"
            />
            <FormControlLabel
              control={
                  <Switch
                    checked={this.state.display_budget}
                    onChange={e => this.setState({ display_budget: e.target.checked })}
                    name="Budget" />}
              label="Budget"
            />
            <FormControlLabel
              control={
                  <Switch
                    checked={this.state.display_category_graphs}
                    onChange={e => this.setState({ display_category_graphs: e.target.checked })}
                    name="Display Category Graphs" />}
              label="Display Category Graphs"
            />
            </FormGroup>
        </CardContent>
        </Card>
        );
    }

    render() {
        console.log('presenting the data: ');
        console.log(this.state.present);
        const { get_budget } = this.props;
        console.log(get_budget);
        var data = [];
        if (this.state.display_budget && get_budget.budget) {
            data.push(...this.add_budget_data(get_budget.budget.budget));
        }
        data.push(...this.state.present);
        console.log('presenting the data: ');
        console.log(data);
        return (
            <Layout PageName="Annual Report">
                <section>
                <Grid
                container
                justify="center"
                alignItems="flex-start"
                spacing={3}>
                    <Grid item md={12}>
                    <Card>
                        <CardContent>
                        <LineChart {...config} data={ this.aggregate(data, this.state.agg) } />
                        </CardContent>
                    </Card>
                    </Grid>
                    <Grid item md={3}>
                    { this.render_categories() }
                    </Grid>
                    <Grid item md={3}>
                    { this.render_agg_buttons() }
                    </Grid>
                </Grid>
                </section>
            </Layout>
        );
    }
}

function mapStateToProps(state) {
    const {  monthly_expenses, get_budget } = state;
    return {
        monthly_expenses,
        get_budget,
    };
}

export default connect(mapStateToProps)(ExpenseChart);
