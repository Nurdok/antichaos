import { expenseConstants } from '../constants/expense';

export function expenses(state = {}, action) {
  switch (action.type) {
    case expenseConstants.GET_REQUEST:
      return {
        loading: true
      };
    case expenseConstants.GET_SUCCESS:
      return {
        items: action.expenses.expenses,
        total: action.expenses.total
      };
    case expenseConstants.GET_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}

export function monthly_expenses(state = {}, action) {
  switch (action.type) {
    case expenseConstants.GET_MONTHLY_REQUEST:
      return {
        loading: true
      };
    case expenseConstants.GET_MONTHLY_SUCCESS:
      return {
        items: action.expenses,
      };
    case expenseConstants.GET_MONTHLY_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}

export function addexpense(state = {}, action) {
  switch (action.type) {
    case expenseConstants.ADD_REQUEST:
      return {
        adding: true
      };
    case expenseConstants.ADD_SUCCESS:
      return {
        added: true
      };
    case expenseConstants.ADD_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}


export function removeexpense(state = {}, action) {
  switch (action.type) {
    case expenseConstants.REMOVE_REQUEST:
      return {
        removing: true
      };
    case expenseConstants.REMOVE_SUCCESS:
      return {
        removed: true
      };
    case expenseConstants.REMOVE_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}
