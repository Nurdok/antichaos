import { combineReducers } from 'redux';

import { authentication } from './authentication';
import { registration } from './registration';
import { users } from './user';
import { alert } from './alerts';
import { addasset, removeasset, assets, asset_history} from './asset';
import { transaction, transactions } from './transaction';
import { addexpense, removeexpense, expenses, monthly_expenses } from './expense';
import { get_budget, set_budget, get_checking_account, set_checking_account } from './settings';
import { contract, add_contract } from './contract';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  addasset,
  removeasset,
  assets,
  transaction,
  transactions,
  addexpense,
  removeexpense,
  expenses,
  monthly_expenses,
  get_budget,
  set_budget,
  get_checking_account,
  set_checking_account,
  asset_history,
  contract,
  add_contract,
});

export default rootReducer;
