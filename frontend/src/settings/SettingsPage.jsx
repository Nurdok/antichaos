import React from 'react';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import { assetActions } from '../actions/asset';
import { settingsActions } from '../actions/settings';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import Layout from '../Layout/Layout';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import FormGroup from '@material-ui/core/FormGroup';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from "@material-ui/core/Grid";
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
        Card: {
            padding: theme.spacing.unit,
            margin: theme.spacing.unit * 2,
            width: "fit-content",
        },
        button: {
          marginTop: theme.spacing.unit * 3,
        },
      });

class SettingsPage extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          income: 0,
          budget: 0,
          savings: 0,
          savings_percent: 0,
          checking_asset: '',
          expenses_date_of_payment: 1,
      }

      this.handleChange = this.handleChange.bind(this);
      this.render_budget_config = this.render_budget_config.bind(this);
  }

  componentDidMount() {
      this.props.dispatch(assetActions.getAssets());
      this.props.dispatch(settingsActions.get_budget());
      this.props.dispatch(settingsActions.get_checking_account());
  }

  componentDidUpdate(PrevProps, PrevStates) {
      const { get_budget: prev_budget,  get_checking_account: prev_asset} = PrevProps;
      const { get_budget: next_budget,  get_checking_account: next_asset} = this.props;

      if (prev_budget.loading && next_budget.budget) {
          let savings = next_budget.budget.income - next_budget.budget.budget;
          let savings_percent = savings / next_budget.budget.income;
          this.setState({
              budget: next_budget.budget.budget,
              income: next_budget.budget.income,
              savings: savings,
              savings_percent: savings_percent,
          });
      }

      if (prev_asset.loading && next_asset.checking_account) {
          this.setState({
              checking_asset: next_asset.checking_account.asset,
              expenses_date_of_payment: next_asset.checking_account.date });
      }
  }

  handleChange(event) {
    let name = event.target.id;
    let value = Number(event.target.value);
    let values_to_update = {};
    if (name === 'income') {
        console.log("update income");
        let new_savings = value - this.state.budget;
        values_to_update['savings'] = new_savings;
        values_to_update['savings_percent'] = new_savings * 100 / value;
    }
    if (name === 'budget') {
        console.log("update budget");
        values_to_update['savings'] = this.state.income - value;
        values_to_update['savings_percent'] = (this.state.income - value) * 100 / this.state.income;
    }
    if (name === 'savings') {
        console.log("update savings");
        values_to_update['budget'] = this.state.income - value;
        values_to_update['savings_percent'] = value * 100 / this.state.income;
    }
    this.setState({ ...values_to_update, [name] : value });
  }

  render_budget_config(classes) {
      return (
          <Card className={classes.Card}>
          <CardHeader title="Budget Configuration" />
          <CardContent>
            <Grid
            container
            direction="column"
            justify="center"
            alignItems="flex-start"
            spacing={3}
            >
            <Grid item>
                <TextField
                    id="income"
                    label="Income"
                    value={this.state.income}
                    onChange={this.handleChange}
                    variant="outlined"
                    type="number" />
            </Grid>
            <Grid item>
                <TextField
                    id="budget"
                    label="Budget"
                    value={this.state.budget}
                    onChange={this.handleChange}
                    variant="outlined"
                    type="number" />
            </Grid>
            <Grid item>
                <TextField
                    id="savings"
                    label="Savings"
                    value={this.state.savings}
                    onChange={this.handleChange}
                    variant="outlined"
                    type="number" />
            </Grid>
            <Grid item>
                <TextField
                    id="Savings Percent"
                    label="Savings Percent"
                    value={ Number((this.state.savings_percent).toFixed(2)) + '%' }
                    disabled
                    variant="outlined" />
            </Grid>
            <Grid item>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={ _ =>
                        this.props.dispatch(
                            settingsActions.set_budget(
                                this.state.income,
                                this.state.budget))
                            }>
                  Save
                </Button>
            </Grid>
            </Grid>
            </CardContent>
          </Card>
      );
  }

  render_main_asset(classes, assets) {
      return (
          <Card className={classes.Card}>
          <CardHeader title="Checking Account" />
          <CardContent>
            <Grid
            container
            direction="column"
            justify="center"
            alignItems="flex-start"
            spacing={3}
            >
            <Grid item>
                <Select
                  autoWidth
                  variant="outlined"
                  value={this.state.checking_asset}
                  onChange={e => this.setState({ checking_asset : e.target.value})}
                  inputProps={{
                    name: 'checking-asset',
                    id: 'checking-asset',
                  }}
                >
                    {assets.items && Object.keys(assets.items).map(
                        asset => (<MenuItem value={asset}>{asset}</MenuItem>))
                    }
                </Select>
            </Grid>
            <Grid item>
                <Select
                  autoWidth
                  variant="outlined"
                  value={this.state.expenses_date_of_payment}
                  onChange={e => this.setState({ expenses_date_of_payment : e.target.value})}
                  inputProps={{
                    name: 'expenses_date_of_payment',
                    id: 'expenses_date_of_payment',
                  }}
                >
                    {Array.from(Array(28).keys()).map(
                        i => (<MenuItem value={i + 1}>{i + 1}</MenuItem>))
                    }
                </Select>
            </Grid>
            <Grid item>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={ _ =>
                        this.props.dispatch(
                            settingsActions.set_checking_account(
                                this.state.checking_asset,
                                this.state.expenses_date_of_payment))
                            }>
                  Save
                </Button>
            </Grid>
            </Grid>
            </CardContent>
          </Card>
      );
  }

  render() {
      const { classes, assets } = this.props;
      return (
          <Layout PageName="Settings">
          <Grid container spacing={2} >
              <Grid item>
              { this.render_budget_config(classes) }
              </Grid>
              <Grid item>
              { this.render_main_asset(classes, assets) }
              </Grid>
          </Grid>
          </Layout>
      );
  }
}

function mapStateToProps(state) {
    const { assets, get_budget, set_budget, get_checking_account, set_checking_account } = state;
    return {
        assets,
        get_budget,
        set_budget,
        get_checking_account,
        set_checking_account,
    };
}

export default withStyles(styles)(connect(mapStateToProps)(SettingsPage));
