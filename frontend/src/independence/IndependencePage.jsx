import React from "react";
import { LineChart } from "@opd/g2plot-react";
import { assetActions } from '../actions/asset';
import { settingsActions } from "../actions/settings";
import { connect } from 'react-redux';
import Layout from '../Layout/Layout';
import Grid from '@material-ui/core/Grid';
import {FinanceUtils} from './utils';
import CardHeader from '@material-ui/core/CardHeader';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

const config = {
    title: {
        visible: true,
        text: "Expected Equity"
    },
    description: {
        visible: true,
        text: "Equity increase with expected interest rate and monthly savings until FIRE."
    },
    padding: "auto",
    forceFit: true,
    xField: "date",
    yField: "equity",
    smooth: true,
    xAxis: {
      type: "time",
      mask: "MMM, YYYY",
      label: {
        visible: true,
        autoHide: true
      }
    },
    animation: {
      enter: {
        animation: "clipingWithData"
      }
  },
  meta: {
      equity: {
        alias: "Equity",
        formatter: (value)=>{ return Math.floor(value / 1000).toLocaleString() + 'k' }
        }
  }

};

class IndependencePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            present: [],
            present_assets: 0,
            monthly_savings: 0,
            interest_rate: 5.0,
            monthly_expenses: 0,
        };
    }

    componentDidMount() {
        this.props.dispatch(assetActions.getAssets());
        this.props.dispatch(settingsActions.get_budget());
    }

    calculate_present_points() {
        console.log('starting calculation');
        const { present_assets, interest_rate, monthly_savings } = this.state;
        const target_equity = FinanceUtils.calculate_target_sum(this.state.monthly_expenses);
        const months = Math.ceil(FinanceUtils.get_months_to_freedom(
            present_assets,
            monthly_savings,
            interest_rate / 100,
            target_equity));

        let present = [];
        let month = new Date();
        month.setDate(1);

        let r_month = FinanceUtils.get_monthly_interest(interest_rate / 100);
        let n = 0;
        let value = present_assets;
        console.log('before loop');
        while (n <= months) {
            present.push({
                 date: month.getTime(),
                 equity: value,
                 type: 'assets' });
             month.setMonth(month.getMonth() + 1);
             n += 1;
             value = (value * (1 + r_month)) + monthly_savings;
        }
        console.log('after loop');
        this.setState({ present: present});
    }

    componentDidUpdate(PrevProps, PrevStates) {
        console.log('start update independence chart');

        if ( this.state.present_assets != PrevStates.present_assets ||
             this.state.monthly_savings != PrevStates.monthly_savings ||
             this.state.interest_rate != PrevStates.interest_rate ||
             this.state.monthly_expenses != PrevStates.monthly_expenses ) {
            this.calculate_present_points();
        }

        const { assets: prev_assets, get_budget: prev_budget } = PrevProps;
        const { assets: next_assets, get_budget: next_budget } = this.props;

        // reset present to show only relevant data
        if (prev_assets.loading == true && next_assets.items) {
            let total = Object.values(next_assets.items).reduce(
                (item, initial) => { return item + initial; }, 0);
            this.setState({ present_assets: total })
        }

        if (prev_budget.loading && next_budget.budget) {
            let savings = next_budget.budget.income - next_budget.budget.budget;
            this.setState({
                monthly_expenses: next_budget.budget.budget,
                monthly_savings: savings,
            });
        }
    }

    renderResultsGraph() {
        return (
            <Card>
                <CardContent>
                <LineChart {...config} data={ this.state.present } />
                </CardContent>
            </Card>);
    }

    renderResultsCard() {
        const { present_assets, interest_rate, monthly_savings } = this.state;
        const target_equity = FinanceUtils.calculate_target_sum(this.state.monthly_expenses);
        const months = Math.ceil(FinanceUtils.get_months_to_freedom(
            present_assets,
            monthly_savings,
            interest_rate / 100,
            target_equity));

        var finish_date = new Date();
        finish_date.setMonth(finish_date.getMonth() + months);
        return (
            <Card>
                <CardActionArea>
                   <CardMedia
                     component="img"
                     height = "inherit"
                     image="/images/freedom.jpg"
                     title="Independence Time"
                   />
                   <CardContent>
                     <Typography gutterBottom variant="h5" component="h2">
                       Independence
                     </Typography>
                     <Typography variant="body2" color="textSecondary" component="p">
                       Target Aquity: { target_equity.toLocaleString() }
                     </Typography>
                     <Typography variant="body2" color="textSecondary" component="p">
                       Independence Day: {finish_date.toLocaleDateString()}
                     </Typography>
                     <Typography variant="body2" color="textSecondary" component="p">
                       Months To Indpendence: {months}
                     </Typography>
                   </CardContent>
                 </CardActionArea>
            </Card>);
    }

    renderOptions() {
        return (
        <Card>
        <CardHeader title="Independence Parameters" />
        <CardContent>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="flex-start"
              spacing={3}>
              <Grid item>
                <TextField
                  autoFocus
                  id="assets"
                  label="Current Equity"
                  value={this.state.present_assets}
                  onChange={e => this.setState({ present_assets :  Number(e.target.value)})}
                  type="number"
                />
              </Grid>
              <Grid item>
                <TextField
                  autoFocus
                  id="interest"
                  label="Interest Rate"
                  value={this.state.interest_rate}
                  onChange={e => this.setState({ intereset_rate : parseFloat(e.target.value)})}
                  type="number"
                />
              </Grid>
              <Grid item>
                <TextField
                  autoFocus
                  id="savings"
                  label="Monthly Savings"
                  value={this.state.monthly_savings}
                  onChange={e => this.setState({ monthly_savings : Number(e.target.value)})}
                  type="number"
                />
              </Grid>
              <Grid item>
                <TextField
                  autoFocus
                  id="expenses"
                  label="Monthly Expenses"
                  value={this.state.monthly_expenses}
                  onChange={e => this.setState({ monthly_expenses : Number(e.target.value)})}
                  type="number"
                />
              </Grid>
            </Grid>
        </CardContent>
        </Card>
      );
    }


    render() {
        return (
            <Layout PageName="Financial Independnece Retire Early">
                <section>
                <Grid
                container
                justify="center"
                spacing={3} >
                    <Grid item md={4}>
                    { this.renderOptions() }
                    </Grid>
                    <Grid item md={6}>
                    { this.renderResultsCard() }
                    </Grid>
                    <Grid item md={10}>
                    { this.renderResultsGraph() }
                    </Grid>
                  </Grid>
                </section>
            </Layout>
        );
    }
}

function mapStateToProps(state) {
    const {  assets, get_budget } = state;
    return {
        assets,
        get_budget,
    };
}

export default connect(mapStateToProps)(IndependencePage);
