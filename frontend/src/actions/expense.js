import { expenseConstants } from '../constants/expense';
import { expenseServices } from '../services/expense';
import { alertActions } from './alerts';

function addExpense(name, amount, category, date) {
    return dispatch => {
        dispatch(request(name, amount, category, date));

        expenseServices.add_expense(name, amount, category, date)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('Expense added successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request(name, amount, category, date) {
        return {
            type: expenseConstants.ADD_REQUEST, name, amount, category, date } }
    function success(users) { return { type: expenseConstants.ADD_SUCCESS} }
    function failure(error) { return { type: expenseConstants.ADD_FAILURE, error } }
}


function RemoveExpense(name, amount, category, date) {
    return dispatch => {
        dispatch(request(name, amount, category, date));

        expenseServices.remove_expense(name, amount, category, date)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('Expense removed successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request(name, amount, category, date) { return {
        type: expenseConstants.REMOVE_REQUEST, name, amount, category, date } }
    function success(users) { return { type: expenseConstants.REMOVE_SUCCESS} }
    function failure(error) { return { type: expenseConstants.REMOVE_FAILURE, error } }
}


function getExpenses(date) {
    return dispatch => {
        dispatch(request(date));

        expenseServices.get_expenses(date)
            .then(
                expenses => dispatch(success(expenses)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: expenseConstants.GET_REQUEST } }
    function success(expenses) { return { type: expenseConstants.GET_SUCCESS, expenses } }
    function failure(error) { return { type: expenseConstants.GET_FAILURE, error } }
}

function getMonthlyExpenses(year) {
    return dispatch => {
        dispatch(request(year));

        expenseServices.get_monthly_expenses(year)
            .then(
                expenses => dispatch(success(expenses)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: expenseConstants.GET_MONTHLY_REQUEST } }
    function success(expenses) { return { type: expenseConstants.GET_MONTHLY_SUCCESS, expenses } }
    function failure(error) { return { type: expenseConstants.GET_MONTHLY_FAILURE, error } }
}

export const expenseActions = {
    addExpense,
    RemoveExpense,
    getExpenses,
    getMonthlyExpenses
};
