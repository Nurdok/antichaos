import { settingsConstants } from '../constants/settings';
import { settingsServices } from '../services/settings';
import { alertActions } from './alerts';

function set_budget(income, budget) {
    return dispatch => {
        dispatch(request(income, budget));

        settingsServices.set_budget(income, budget)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('budget set successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request(income, budget) {
        return {
            type: settingsConstants.SET_BUDGET_REQUEST, income, budget } }
    function success(users) { return { type: settingsConstants.SET_BUDGET_SUCCESS} }
    function failure(error) { return { type: settingsConstants.SET_BUDGET_FAILURE, error } }
}

function get_budget() {
    return dispatch => {
        dispatch(request());

        settingsServices.get_budget()
            .then(
                budget => {
                    dispatch(success(budget));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request() { return { type: settingsConstants.GET_BUDGET_REQUEST } }
    function success(budget) { return { type: settingsConstants.GET_BUDGET_SUCCESS, budget } }
    function failure(error) { return { type: settingsConstants.GET_BUDGET_FAILURE, error } }
}

function set_checking_account(asset, date) {
    return dispatch => {
        dispatch(request(asset, date));

        settingsServices.set_checking_account(asset, date)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('main account set successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request(asset) {
        return {
            type: settingsConstants.SET_CHECKING_ACCOUNT_REQUEST, asset, date } }
    function success(users) { return { type: settingsConstants.SET_CHECKING_ACCOUNT_SUCCESS} }
    function failure(error) { return { type: settingsConstants.SET_CHECKING_ACCOUNT_FAILURE, error } }
}

function get_checking_account() {
    return dispatch => {
        dispatch(request());

        settingsServices.get_checking_account()
            .then(
                checking_account => {
                    dispatch(success(checking_account));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request() { return { type: settingsConstants.GET_CHECKING_ACCOUNT_REQUEST } }
    function success(checking_account) { return { type: settingsConstants.GET_CHECKING_ACCOUNT_SUCCESS, checking_account } }
    function failure(error) { return { type: settingsConstants.GET_CHECKING_ACCOUNT_FAILURE, error } }
}

export const settingsActions = {
    get_budget,
    set_budget,
    get_checking_account,
    set_checking_account,
};
