import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import MobileStepper from '@material-ui/core/MobileStepper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import { assetActions } from '../actions/asset';
import { transactionActions } from '../actions/transaction';


const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  formControl: {
      margin: theme.spacing.unit,
      minWidth: 120,
  },
  paper: {
      marginTop: theme.spacing.unit * 2,
      display: 'flex',
//      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  }
});

class ProgressMobileStepper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeStep: 0,
            tag: '',
            action: 'TRANSFER',
            amount: '',
            source: '',
            target: '',
            period: 0,
            start_time: 0,
            periods: 0
        }

        this.handleNext = this.handleNext.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.display_step = this.display_step.bind(this);
        this.render_action = this.render_action.bind(this);
        this.render_assets = this.render_assets.bind(this);
        this.render_period = this.render_period.bind(this);
        this.complete = this.complete.bind(this);
        this.cancel = this.cancel.bind(this);
        this.render_large_screen = this.render_large_screen.bind(this);
   }

   complete() {
       const { dispatch, handleClose } = this.props;
       let transaction = { name: this.state.action ,
                           tag: this.state.tag,
                           amount: this.state.amount };

       if (['TRANSFER', 'WITHDRAW'].includes(transaction.name)) {
           transaction.source = this.state.source;
       }

       if (['TRANSFER', 'DEPOSIT'].includes(transaction.name)) {
           transaction.target = this.state.target;
       }

       let periodic_transaction = { transaction }
       periodic_transaction.period = this.state.period;
       periodic_transaction.start_time = this.state.start_time;
       periodic_transaction.periods = this.state.periods;

       dispatch(transactionActions.sendPeriodTransaction(periodic_transaction));
       handleClose();
   }

   cancel() {
       this.setState({
           activeStep: 0,
           action: 'TRANSFER',
           amount: '',
           source: '',
           target: '',
           period: 0,
           start_time: 0,
           periods: 0
       });

       this.props.handleClose()
   }
    handleNext() {
        this.setState(state => ({
          activeStep: state.activeStep + 1,
        }));
    }

    handleBack() {
        this.setState(state => ({
          activeStep: state.activeStep - 1,
        }));
    }

    componentDidMount() {
        this.props.dispatch(assetActions.getAssets());
    }

    render_action() {
        const { classes } = this.props;
        return (
            <div>
            <RadioGroup
              aria-label="action"
              name="action"
              value={this.state.action}
              onChange={event => this.setState({ action: event.target.value })}
            >
              <FormControlLabel value="TRANSFER" control={<Radio />} label="Transfer" />
              <FormControlLabel value="WITHDRAW" control={<Radio />} label="Withdraw" />
              <FormControlLabel value="DEPOSIT" control={<Radio />} label="Deposit" />
            </RadioGroup>
            <TextField
              autoFocus
              id="tag"
              label="tag"
              onChange={e => this.setState({ tag : e.target.value})}
            />
            </div>

        );
    }

    render_assets() {
        const { classes, assets } = this.props;
        return (
            <div>
            { (this.state.action === 'TRANSFER' || this.state.action == 'WITHDRAW') &&
                <FormControl className={classes.formControl}>
                <InputLabel htmlFor="source-asset">Source</InputLabel>
                <Select
                  value={this.state.source}
                  onChange={e => this.setState({ source : e.target.value})}
                  inputProps={{
                    name: 'source-asset',
                    id: 'source-asset',
                  }}
                >
                    {assets.items && Object.keys(assets.items).filter(
                                asset => asset !== this.state.target).map(
                                    asset => (<MenuItem value={asset}>{asset}</MenuItem>))
                    }
                </Select>
                </FormControl>
            }
            { (this.state.action === 'TRANSFER' || this.state.action == 'DEPOSIT') &&
                <FormControl className={classes.formControl}>
                <InputLabel htmlFor="target-asset">Target</InputLabel>
                <Select
                  value={this.state.target}
                  onChange={e => this.setState({ target : e.target.value})}
                  inputProps={{
                    name: 'target-asset',
                    id: 'target-asset',
                  }}
                >
                    {assets.items && Object.keys(assets.items).filter(
                                asset => asset !== this.state.source).map(
                                    asset => (<MenuItem value={asset}>{asset}</MenuItem>))
                    }
                </Select>
                </FormControl>
            }
            <FormControl className={classes.formControl}>
            <TextField
              autoFocus
              id="amount"
              label="amount"
              onChange={e => this.setState({ amount : Number(e.target.value)})}
              type="number"
            />
            </FormControl>
            </div>
        );
    }

    render_period() {
        const { classes } = this.props;
        return (
            <div>
            <FormControl className={classes.formControl}>
            <TextField
                id="date"
                label="Start Date"
                type="date"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={e => this.setState({ start_time: e.target.valueAsNumber})}
              />
              </FormControl>
              <FormControl className={classes.formControl}>
            <TextField
                autoFocus
                id="periods"
                label="periods"
                onChange={e => this.setState({ periods : Number(e.target.value)})}
                type="number"
              />
              </FormControl>
              <FormControl className={classes.formControl}>
              <InputLabel htmlFor="repeated">Repeat</InputLabel>
              <Select
                value={this.state.period}
                onChange={e => this.setState({ period : e.target.value})}
                inputProps={{
                  name: 'repeated',
                  id: 'repeated',
                }}
              >
                  <MenuItem value='Daily'> Daily </MenuItem>
                  <MenuItem value='Weekly'> Weekly </MenuItem>
                  <MenuItem value='Monthly'> Monthly </MenuItem>
              </Select>
              </FormControl>
              </div>
        );
    }

    display_step(assets) {
        const { activeStep: step } = this.state;
        switch (step) {
        case 0:
            return this.render_action();
        case 1:
            return this.render_assets();
        case 2:
            return this.render_period();
        default:
          return 'Unknown step';
        }
    }

    render_large_screen() {
        const { classes, theme, assets } = this.props;

        return (
            <Card>
            <CardContent>
            <div style={{display:'flex', flexDirection:'row'}}>
            { this.render_action() }
            { this.render_assets() }
            { this.render_period() }
            </div>
            <Button size="small" onClick={this.cancel}>
              Cancel
            </Button>
            <Button size="small" onClick={this.complete}>
              Complete
            </Button>
            </CardContent>
            </Card>)
    }

    render() {
        const { classes, assets, theme } = this.props;
        console.log(this.props.width);
        if (isWidthUp('sm', this.props.width)) {
            return this.render_large_screen();
        }

        return (
        <Card>
            <MobileStepper
            variant="progress"
            steps={3}
            position="static"
            activeStep={this.state.activeStep}
            className={classes.root}
            nextButton={
                this.state.activeStep === 2 ?
                    <Button size="small" onClick={this.complete}>
                      Complete
                    </Button>
                :
                  <Button size="small" onClick={this.handleNext} disabled={this.state.activeStep === 5}>
                    Next
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                  </Button>
            }
            backButton={
                this.state.activeStep == 0 ?
                    <Button size="small" onClick={this.cancel}>
                      Cancel
                    </Button>
                :
                  <Button size="small" onClick={this.handleBack} disabled={this.state.activeStep === 0}>
                    {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                    Back
                  </Button>
            }>

            </MobileStepper>
            <CardContent>
            {this.display_step(assets)}
            </CardContent>
        </Card>
        );
    }
}

ProgressMobileStepper.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  handleClose: PropTypes.object.isRequired,
  width: PropTypes.object.isRequired,
};


function mapStateToProps(state) {
    const { assets } = state;
    return {
        assets
    };
}


export default withWidth()(withStyles(styles, { withTheme: true })(connect(mapStateToProps)(ProgressMobileStepper)));
