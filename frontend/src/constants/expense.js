export const expenseConstants = {
    GET_REQUEST: 'EXPENSE_GET_REQUEST',
    GET_SUCCESS: 'EXPENSE_GET_SUCCESS',
    GET_FAILURE: 'EXPENSE_GET_FAILURE',

    GET_MONTHLY_REQUEST: 'EXPENSE_GET_MONTHLY_REQUEST',
    GET_MONTHLY_SUCCESS: 'EXPENSE_GET_MONTHLY_SUCCESS',
    GET_MONTHLY_FAILURE: 'EXPENSE_GET_MONTHLY_FAILURE',

    ADD_REQUEST: 'EXPENSE_ADD_REQUEST',
    ADD_SUCCESS: 'EXPENSE_ADD_SUCCESS',
    ADD_FAILURE: 'EXPENSE_ADD_FAILURE',

    REMOVE_REQUEST: 'EXPENSE_REMOVE_REQUEST',
    REMOVE_SUCCESS: 'EXPENSE_REMOVE_SUCCESS',
    REMOVE_FAILURE: 'EXPENSE_REMOVE_FAILURE',
};

export const expenseCategories = {categories: ['Groceries', 'Health',
                                               'Restaurant', 'Leisure',
                                               'Transport', 'Gift',
                                               'Shopping', 'Trips',
                                               'Pet', 'Bills']};
