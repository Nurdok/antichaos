import { authHeader } from '../helpers/auth-header';
import { userService } from './user';

export const contractServices = {
    add_contract,
    get_contract,
};


function add_contract(contract) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ ...contract })
    };

    return fetch('/data/set_contract', requestOptions).then(handleResponse)
}


function get_contract() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/data/get_contract', requestOptions)
        .then(handleResponse)
        .then(contract => {
            return contract;
        });
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        console.log("got response");
        console.log(response);
        if (!response.ok) {
            console.log("not ok response");
            if (response.status === 401) {
                console.log("really 401");
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        console.log("finished");

        return data.data;
    });
}
