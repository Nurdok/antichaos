import { authHeader } from '../helpers/auth-header';
import { userService } from './user';

export const assetServices = {
    add_asset,
    remove_asset,
    get_assets,
    get_asset_history
};


function add_asset(asset, amount) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ asset, amount })
    };

    return fetch('/data/assets', requestOptions).then(handleResponse)
}


function remove_asset(asset, amount) {
    const requestOptions = {
        method: 'DELETE',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ asset })
    };

    return fetch('/data/assets', requestOptions).then(handleResponse)
}


function get_assets() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/data/assets', requestOptions)
        .then(handleResponse)
        .then(assets => {
            return assets;
        });
}

function get_asset_history(asset) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/data/history?asset=' + asset, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
        });
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        console.log("got response");
        console.log(response);
        if (!response.ok) {
            console.log("not ok response");
            if (response.status === 401) {
                console.log("really 401");
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        console.log("finished");

        return data.data;
    });
}
