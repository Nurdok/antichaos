import { authHeader } from '../helpers/auth-header';
import { userService } from './user';

export const transactionServices = {
    deposit,
    withdraw,
    transfer,
    get_periodic_transactions,
    send_periodic_transaction
};


function deposit(target, amount, tag) {
    let name = 'DEPOSIT';
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ name, target, amount, tag })
    };

    return fetch('/data/transaction', requestOptions).then(handleResponse)
}


function withdraw(source, amount, tag) {
    let name = 'WITHDRAW';
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ name, source, amount, tag })
    };

    return fetch('/data/transaction', requestOptions).then(handleResponse)
}


function transfer(source, target, amount, tag) {
    let name = 'TRANSFER';
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ name, source, target, amount, tag })
    };

    return fetch('/data/transaction', requestOptions).then(handleResponse)
}


function get_periodic_transactions() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/data/periodic', requestOptions)
        .then(handleResponse)
        .then(transactions => {
            return transactions;
        })
}


function send_periodic_transaction(transaction) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(transaction)
    };

    return fetch('/data/periodic', requestOptions).then(handleResponse)
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data.data;
    });
}
